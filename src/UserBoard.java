import java.util.ArrayList;
import java.util.Random;

public class UserBoard {
//    public static final int WIN = 10;
//    public static final int LOSE = -10;
//    public static final int DRAW = 5;
     static final int PLAYER_X = 1;
     static final int PLAYER_Y = -1;
     static final int NOPLAYER = 0;
     int[][] board;
    private int size;
    Random random = new Random();
    private String value = "-";
    private Point computerPoint,computerWinpoint,computerLosePoint;

    public UserBoard() {
        board = new int[3][3];
        size = board.length;
    }
    //checking game State.,
    public boolean isGameOver() {
        boolean isEnded=false;
         if(isPlayerWin(PLAYER_X)){
            System.out.println("You Win!!!");
            isEnded=true;
        } if (isPlayerWin(PLAYER_Y)){
            System.out.println("You Lose!!!");
            isEnded=true;
        } if (getAvailablePoints().isEmpty()){
            System.out.println("DRAW");
            isEnded=true;
        }
        return isEnded;
    }
    // checking  win  state of (O) or (X)
    public boolean isPlayerWin(int player) {
        boolean isNotContinuable=false;
        boolean result;
      for(int i=0;i<size;i++){
            result=true;
            for(int j=0;j<board[i].length;j++){
                if(board[i][j]!=player){
                    result=false;
               break;
                }
            }
            if(result){
               isNotContinuable=true;
               break;
            }

   }
        for(int i=0;i<size;i++){
            result=true;
            for(int j=0;j<board[i].length;j++){
                if(board[j][i]!=player){
                    result=false;
                    break;
                }
            }
            if(result){
                isNotContinuable=true;
                break;
            }

        }
// Correct Algorithm???
        if((board[0][0]== board[1][1] && board[0][0]==board[2][2] && board[0][0]==player) ||
                board[0][2]==board[1][1] && board[0][2]== board[2][0] && board[0][2]==player){
            isNotContinuable=true;
        }
        return isNotContinuable;
    }
// getting all the (-) available places on the board
    public ArrayList<Point> getAvailablePoints() {
        ArrayList<Point> pointsToAvailable = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (board[i][j] == NOPLAYER) {
                    pointsToAvailable.add(new Point(i, j));
                }
            }
        }
        return pointsToAvailable;
    }
    // placing a point on the Board
    public void move(Point point, int player) {
        board[point.x][point.y] = player;

    }
    //Computer Simple AI
    public Point getBestPoint() {

        ArrayList<Point> pointsAvailable = getAvailablePoints();
        for (int i = 0; i < pointsAvailable.size(); i++) {
            Point point = pointsAvailable.get(i);
            move(point, PLAYER_Y);
            if (isPlayerWin(PLAYER_Y)) {
                move(point, NOPLAYER);
                return point;
            }
            move(point,NOPLAYER);
        }
        for(int i=0;i<pointsAvailable.size();i++){
            Point point=pointsAvailable.get(i);
                move(point,PLAYER_X);
                if(isPlayerWin(PLAYER_X)){
                    move(point,NOPLAYER);
                    return point;
                }
                move(point,NOPLAYER);

            }
if(!pointsAvailable.isEmpty())
             computerPoint=pointsAvailable.get(random.nextInt(pointsAvailable.size()-1));
return computerPoint;
        }
        //Just printing a Board
    public void showBoard() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (board[i][j] == PLAYER_X) {
                    value = "X";
                } else if (board[i][j] == PLAYER_Y) {
                    value = "O";
                } else if (board[i][j] == NOPLAYER) {
                    value = "*";
                }
                System.out.print(value + " ");
            }
            System.out.println();
        }
        System.out.println();
    }


}